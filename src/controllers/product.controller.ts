import {  Request, Response } from 'express';
import { networkSuccess, networkError, serverError } from '../middlewares/response.middleware'
import Product from '../models/product.model';

const create = async (req: Request, res: Response) => {
    try {

        const codigo = await Product.findOne({codigo: req.body.codigo});
        if (codigo){ 
            networkError({res,message:'El codigo ya existe', status:400, error: {}});
            return;
        }
        const product = new Product(req.body)

        await product.save()
        networkSuccess({res, message:  'Producto creado correctamente', data: product})
        return;

    } catch (error) {
        serverError({res, message: 'Ha ocurrido un problema', error});
    }
};

const update = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const product = await Product.findOne({id});
        //await Product.updateOne({codigo:req.body.codigo},{categoria:req.body.categoria},{marca:req.body.marca},{modelo:req.body.modelo},{img:req.body.img},{talla:req.body.talla},{color:req.body.color},{precio:req.body.precio},{detalle:req.body.detalle},{genero:req.body.genero});
        if (product){ 
            const productUpdate = {
                //@ts-ignore
                codigo: req.body.codigo ?? product.codigo,
                //@ts-ignore
                categoria: req.body.categoria ?? product.categoria,
                //@ts-ignore
                marca: req.body.marca ?? product.marca,
                //@ts-ignore
                modelo: req.body.modelo ?? product.modelo,
                //@ts-ignore
                img: req.body.img ?? product.img,
                //@ts-ignore
                talla: req.body.talla ?? product.talla,
                //@ts-ignore
                color: req.body.color ?? product.color,
                //@ts-ignore
                precio: req.body.precio ?? product.precio,
                //@ts-ignore
                detalle: req.body.detalle ?? product.detalle,
                //@ts-ignore
                genero: req.body.genero ?? product.genero,
                //@ts-ignore
                usuarioCreador: req.body.usuarioCreador ?? product.usuarioCreador,
                

            }

            await Product.updateOne({_id: req.body._id},{...productUpdate});
            networkSuccess({res,message:'Producto actualizado correctamente', data: productUpdate});
            return;
        } else {
            networkError({res, message: 'Producto NO encontrado', status: 404})
        }

    } catch (error) {
        serverError({res, message: 'Ha ocurrido un problema', error});
    }
};

const getAll = async (_: Request, res: Response) => {
    try {
        const products = await Product.find(); 
        networkSuccess({res, message:  'Lista de productos obtenida', data:products})
        return;

    } catch (error) {
        serverError({res, message:  'Lista de productos no obtenida', error})
    }
}

const getById= async (req: Request, res: Response) => {
	try {
		const { id } = req.params;
		const product = await Product.findById(id);
		if(product){
			networkSuccess({res,message: 'Producto Encontrado', data: product}) 
			return;
		}else {
			networkError({res, message: 'Producto No encontrado', status: 404});
		}
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema', error});
    }
};

const getDetails= async (req: Request, res: Response) => {
	try {
		const { id } = req.params;
		const product = await Product.findById(id);
		if(product){
            const productDetails = {
                //@ts-ignore
                img: product.img,
                //@ts-ignore
                marca: product.marca,
                //@ts-ignore
                modelo: product.modelo,
                 //@ts-ignore
                 codigo: product.codigo,
                //@ts-ignore
                precio: product.precio,
                //@ts-ignore
                descuento: product.descuentoCampanha,
                //@ts-ignore
                precioFinal: product.precio - product.descuentoCampanha
                

            }
			networkSuccess({res,message: 'Producto Encontrado', data: productDetails}) 
			return;
		}else {
			networkError({res, message: 'Producto No encontrado', status: 404});
		}
	} catch (error) {
		serverError({res, message: 'Ha ocurrido un problema', error});
    }
};

const getByCategory = async (req: Request, res: Response) => {
    try {
        const { categoria } = req.params;
        const products = await Product.find({categoria})
        if (products.length > 0){
            networkSuccess({res, message:  'Producto encontrado', data: products})
            return;
        } else {
            networkError({res, message: 'Producto NO encontrado', status: 404})
        }
    } catch (error) {
        serverError({res, message: 'Ha ocurrido un problema', error})
        
    }
};


const getByBrand = async (req: Request, res: Response) => {
    try {
        const { marca } = req.params;
        const products = await Product.find({marca})
        if (products.length > 0){
            networkSuccess({res, message:  'Producto encontrado', data: products})
            return;
        } else {
            networkError({res, message: 'Producto NO encontrado', status: 404})
        }
    } catch (error) {
        serverError({res, message: 'Ha ocurrido un problema', error})
        
    }
};



export {
    getAll, create, update, getById, getByBrand, getByCategory, getDetails
}
