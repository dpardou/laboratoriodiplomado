import mongoose from "mongoose";

const dbConnection = async() => {
    try {
        await mongoose.connect(process.env.DB_CONNECTION);

        console.log("Database Successfully Connected")

    } catch (error) {
        throw new Error('Error en la conexión, contactar al administrador');
        
    }
};

export default dbConnection;