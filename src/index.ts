import express from 'express';
import { config } from 'dotenv';
import dbConnection from './database/config.database';
import productRoutes from './routes/product.routes';

config();
dbConnection();

const app = express();
app.use(express.json());

app.get('/', (_, response) => {
   response.send({message: "Server Running Successfully"})
}) 

app.use('/api/products', productRoutes);

app.listen(process.env.PORT || 5000, () => {
	console.log(`server running on PORT: ${process.env.PORT || 5000}`)
});
