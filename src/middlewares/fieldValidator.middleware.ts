import {  Request, Response, NextFunction} from 'express';
import {  validationResult  } from 'express-validator';
import { networkError } from './response.middleware';

const fieldValidator = (req: Request, res: Response, next:NextFunction) => {
    const errors = validationResult(req);
    if (  !errors.isEmpty()  ){
        const keys = Object.keys(errors.mapped());
        const errorObject = keys.map((param) => {
            return {
                campo: param,
                mensaje: errors.mapped()[param].msg
            }
        });
    networkError({
        res,
        status: 400,
        message: 'Faltan datos o datos invalidos',
        error: errorObject
    });
    return;
}

    next();
}

export default fieldValidator;