import  { model, Schema } from 'mongoose';

const ProductSchema = new Schema({
    codigo: {
        type: String,
        required: true
    },
    categoria: {
        type: String,
        required: true
    }, 
    marca: {
        type: String,
        required: true
    },
    modelo: {
        type: String,
        required: true
    },
    img: {
        type: String,   
    },
    talla: {
        type: Number,
        required: true,
    },
    color: {
        type: String,
    },
    precio: {
        type: Number,
        required: true
    },
    detalle: {
        type: String
    },
    genero: {//hombre, mujer, niño, unisex, e
        type: String,
        required: true
    },
    campanha: {//si aplica o no campaña
        type: Boolean,
    },
    fechaDesdeCampanha: { //Facha inicio campaña
        type: Date,
    },
    fechaHastaCampanha: { //Fecha finalización campaña
        type: Date,
    },
    descuentoCampanha: { //Descuento % asociado a la campaña 
        type: Number,
    },
    precioFinal: {
        type: Number,
    },
    fechaCreacion: {
        type: Date,
        default: Date.now()
    },
    usuarioCreador: {
        type: String,
        required: true
    },
});

export default model('product', ProductSchema)