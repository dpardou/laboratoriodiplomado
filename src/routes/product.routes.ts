//path: api/products

import {  Router  } from 'express';
import  {  check  } from 'express-validator';
import {  create, getAll, getById, getByBrand, getByCategory, update, getDetails  } from '../controllers/product.controller';
import fieldValidator from '../middlewares/fieldValidator.middleware';

const router = Router();

router.post('/', [
    check('codigo', 'El campo código es requerido').notEmpty(),
    check('categoria', 'El campo categoría es requerido').notEmpty(),
    check('marca', 'El campo marca es requerido').notEmpty(),
    check('modelo', 'El campo modelo es requerido').notEmpty(),
    check('talla', 'El campo talla No es valido').isInt(),
    check('color', 'El campo color es requerido').notEmpty(),
    check('precio', 'El campo precio No es valido').isInt(),
    check('genero', 'El campo genero es requerido').notEmpty(),
    check('usuarioCreador', 'El campo Usuario Creador es requerido').notEmpty(),
    fieldValidator
], create);

router.get('/', getAll);
router.get('/:id', getById);
router.get('/details/:id', getDetails);
router.get('/search/category/:categoria', getByCategory);
router.get('/search/brand/:marca', getByBrand);
router.post('/:id', update);



export default router;
